require 'phash'
require 'find'
image = []
text = []
video = []
music = []
t = 0.2
a = 0.3
v = 0.6
i = 0.7




filename = ARGV[0]

puts "Going to open '#{filename}'"

Dir.glob("#{filename}**/*", File::FNM_DOTMATCH).each do |path|
	next if File.directory?(path)
	image << path if (path =~ /.*\.bmp$/) or (path =~ /.*\.jpg$/) or (path =~ /.*\.gif$/) or (path =~ /.*\.tif$/)
	text << path if (path =~ /.*\.txt$/) or (path =~ /.*\.doc$/) or (path =~ /.*\.docx$/)
	video << path if (path =~ /.*\.avi$/) or (path =~ /.*\.mpeg$/) or (path =~ /.*\.mp4$/)
	music << path if (path =~ /.*\.mp3$/) or (path =~ /.*\.ape$/) or (path =~ /.*\.wav/)
end
def findText(text,t)
	i = 0
	j = 0
	while i < text.size-1
		j = i +1
		while j < text.size 	
			p = Phash::Text.new(text[i]).similarity(Phash::Text.new(text[j]))
			if p > t then	puts  '===' + text[i] + '	' + text[j] +  '	' + p.to_s + '	T'
			end
			j += 1
		end
		i +=1
	end
end
def findVideo(video,v)
	i = 0
	j = 0
	while i < video.size-1
		j = i +1
		while j < video.size 	
			p = Phash::Video.new(video[i]).similarity(Phash::Video.new(video[j]))
			if p > v then	puts  '===' + video[i] + '	' + video[j] +  '	' + p.to_s + '	V'
			end
			j += 1
		end
		i +=1
	end
end
def findMusic(music,a)
	i = 0
	j = 0
	while i < music.size-1
		j = i +1
		while j < music.size 	
			p = Phash::Audio.new(music[i]).similarity(Phash::Audio.new(music[j]))
			if p > a then	puts  '===' + music[i] + '	' + music[j] +  '	' + p.to_s + '	T'
			end
			j += 1
		end
		i +=1
	end
end
def findImage(image,i)
	index = 0
	j = 0
	while index < image.size-1
		j = index +1
		while j < image.size 	
			p = Phash::Image.new(image[index]).similarity(Phash::Image.new(image[j]))
			if p > i then	puts  '===' + image[index] + '	' + image[j] +  '	' + p.to_s + '	I'
			end
			j += 1
		end
		index +=1
	end
end



#puts Phash::Image.new(image1).similarity(Phash::Image.new(image2))

findImage(image, i)
findMusic(music, a)
findVideo(video, v)
findText(text,t)
