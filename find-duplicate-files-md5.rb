require 'digest/md5'

hash = {}
if ARGV.length != 1
    puts "We need exactly one parameter. The name of a file."
    exit;
end
 
filename = ARGV[0]
puts "Going to open '#{filename}'"

Dir.glob("#{filename}**/*", File::FNM_DOTMATCH).each do |f|
  next if File.directory?(f)
  puts f + "sadas"
  key = Digest::MD5.hexdigest(IO.read(f)).to_sym
  if hash.has_key?(key) then hash[key].push(f) else hash[key] = [f] end
end

hash.each_value do |a|
  next if a.length == 1
  puts '=== Identical Files ==='
  a.each { |f| puts "\t" + f }
end
